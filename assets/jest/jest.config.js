require("dotenv").config({ path: "./.env.test" });

module.exports = {
  collectCoverage: true,
  globals: {
    "ts-jest": {
      tsConfig: "tsconfig.json",
    },
  },
  moduleFileExtensions: ["ts", "js"],
  transform: {
    "^.+\\.ts$": "ts-jest",
  },
  testEnvironment: "node",
  testMatch: ["**/__tests__/**/*.test.ts"],
};
