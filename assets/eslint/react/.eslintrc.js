module.exports = {
  parser: "@typescript-eslint/parser",
  extends: [
    "airbnb-base",
    "plugin:@typescript-eslint/recommended",
    "prettier/@typescript-eslint",
    "plugin:prettier/recommended",
    "plugin:react/recommended",
  ],
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: "module",
  },
  plugins: [
    "react",
    "import",
    "jsdoc",
  ],
  rules: {
    "react/prop-types": 0,

    "import/prefer-default-export": 0,

    "jsdoc/check-alignment": 1,
    "jsdoc/check-param-names": 1,
    "jsdoc/check-tag-names": 1,
    "jsdoc/implements-on-classes": 1,
    "jsdoc/no-undefined-types": 1,
    "jsdoc/no-types": 1,
    "jsdoc/require-description": 1,
    "jsdoc/require-jsdoc": 1,
    "jsdoc/require-param": 1,
    "jsdoc/require-param-description": 1,
    "jsdoc/require-param-name": 1,
    "jsdoc/require-returns-check": 1,
    "jsdoc/require-returns-description": 1,
    "jsdoc/valid-types": 1,
    "jsdoc/require-jsdoc": ["error", {
      require: {
        FunctionDeclaration: true,
        ArrowFunctionExpression: true,
      }
    }],
    "jsdoc/require-jsdoc": ["error", {
      require: {
        FunctionDeclaration: true,
        ArrowFunctionExpression: true,
      }
    }],
    "import/extensions": ["error", "ignorePackages", {
      js: "never",
      ts: "never",
      jsx: "never",
      tsx: "never",
    }],
  },
  settings: {
    "import/parsers": {
      "@typescript-eslint/parser": [".ts"]
    },
    "import/resolver": {
      node: {
        extensions: [".js", ".ts"],
      },
      ts: {
        "alwaysTryTypes": true,
      },
    },
    react: {
      pragma: "React",
      version: "detect"
    }
  },
  env: {
    jest: true,
    node: true,
    browser: true,
    es6: true,
  },
  globals: {
    chrome: true,
  }
};
