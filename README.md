# Cubabit ES Policy

Use this in your new projects to pull in a shared and versioned:

- Typescript config
- Eslint config
- Prettier config
- Jest config

## Usage

Currently node, node-module and react projects are supported.

```bash
npx cubabit-set-up-project --type [node,node-module,react]
```

This will:

- Insert scripts into your *package.json* scripts sections for compilation, testing, linting and building
- Create symlinks for typescript, eslint and prettier configs
- Create a symlink to jest configuration
- Add these symlinks to *.gitignore*
- Run the script in *postinstall* so that the symlinks are there when the project is checked-out and deps installed with *yarn*

The relevant dependencies (*typescript*, *eslint*, *jest*, *webpack*, *prettier* etc) are installed as deps of this library so you do not need to add them to your project.

The script will check for existing changes, so can generally be run safely more than once.
