#! /usr/bin/env node
const argv = require("yargs")
  .usage("Usage: $0 [options]")
  .options({
    type: { choices: [ "node", "node-module", "react" ], demandOption: true, alias: "t" },
    update: { type: "boolean" },
  })
  .argv;
const fs = require("fs");
require("colors");

const SOURCE_ASSETS_DIR = `${__dirname}/../assets`;
const TARGET_PROJECT_DIR = process.cwd();

const copyConfigs = () => {
  const linkFile = (source, dest) => {
    try {
      fs.symlinkSync(`${SOURCE_ASSETS_DIR}/${source}`, `${TARGET_PROJECT_DIR}/${dest}`);
      console.log(`  🔗 Linking ${dest}`.green);
    }
    catch (error) {
      if (error.message.match(/^EEXIST: file already exists/)) {
        console.log(`  🔗 ${dest} already linked`.yellow);
      }
      else {
        throw error;
      }
    }
  };

  switch (argv.type) {
    case "node":
      console.log("📋 Setting up config for node project".green);
      linkFile("tsconfig/node/tsconfig.json", "tsconfig.json");
      linkFile("eslint/node/.eslintrc.js", ".eslintrc.js");
      break;
    case "node-module":
      console.log("📋 Setting up config for node-module project".green);
      linkFile("tsconfig/node-module/tsconfig.json", "tsconfig.json");
      linkFile("eslint/node/.eslintrc.js", ".eslintrc.js");
      break;
    case "react":
      console.log("📋 Setting up config for react project".green);
      linkFile("tsconfig/react/tsconfig.json", "tsconfig.json");
      linkFile("eslint/react/.eslintrc.js", ".eslintrc.js");
      break;
  }

  linkFile("prettier/prettier.config.js", "prettier.config.js");
  linkFile("jest/jest.config.js", "jest.config.js");
};

const configurePackageJson = () => {
  console.log("⚙️ Configuring package.json".green);

  const packageJsonFilePath = `${TARGET_PROJECT_DIR}/package.json`;
  const packageJsonSource = fs.readFileSync(packageJsonFilePath, "utf-8");
  const packageJson = JSON.parse(packageJsonSource);

  const setValue = (parent, key, value) => {
    if (typeof parent[key] !== "undefined") {
      console.log(`  ✏️ Not setting '${key}' as already set`.yellow);
    }
    else {
      parent[key] = value;
      console.log(`  ✏️ Set '${key}'`.green);
    }
  };

  if (typeof packageJson.scripts === "undefined") {
    packageJson.scripts = {};
  }

  setValue(packageJson.scripts, "build", "npm run lint && npm run test:ci && npm run compile");
  setValue(packageJson.scripts, "build:watch", "concurrently -k -p \"[{name}]\" -n \"Lint,TypeScript\" -c \"yellow.bold,cyan.bold\" \"npm run lint:watch\" \"npm run compile:watch\"");
  setValue(packageJson.scripts, "test", "TZ=UTC jest --coverage --detectOpenHandles --passWithNoTests");
  setValue(packageJson.scripts, "test:watch", "TZ=UTC jest --watch --coverage");
  setValue(packageJson.scripts, "test:ci", "jest --ci --runInBand --reporters=default --reporters=jest-junit --passWithNoTests");
  setValue(packageJson.scripts, "compile", "tsc");
  setValue(packageJson.scripts, "compile:watch", "tsc -w");
  setValue(packageJson.scripts, "lint:fix", "npm run lint -- --fix");
  setValue(packageJson.scripts, "prepare", `cubabit-set-up-project --type ${argv.type}`);

  if (argv.type === "react") {
    setValue(packageJson.scripts, "lint", "eslint 'src/**/*.{ts,tsx,js,jsx}'");
    setValue(packageJson.scripts, "lint:watch", "esw -w --ext ts,tsx,js,jsx --color --clear src/");
  } else {
    setValue(packageJson.scripts, "lint", "eslint 'src/**/*.{ts,js}'");
    setValue(packageJson.scripts, "lint:watch", "esw -w --ext ts,js --color --clear src/");
  }

  fs.writeFileSync(
    packageJsonFilePath,
    JSON.stringify(packageJson, null, 2),
    { encoding: "utf-8" },
  );
};

const configureGitignore = () => {
  console.log("🚫️ Adding files to .gitignore".green);

  const gitignoreFilePath = `${TARGET_PROJECT_DIR}/.gitignore`;
  let gitignoreSource = "";

  if (fs.existsSync(gitignoreFilePath)) {
    gitignoreSource = fs.readFileSync(gitignoreFilePath, "utf-8");

    if (!/\n$/.test(gitignoreSource)) {
      gitignoreSource += "\n";
    }
  }

  const ignoreFiles = [
    "/.eslintrc.js",
    "/prettier.config.js",
    "/tsconfig.json",
    "/jest.config.js",
    "/coverage",
    "/junit.xml",
    "/test.log",
  ];
  if (argv.type === "node") {
    ignoreFiles.push(
      "/.serverless",
      "/.build",
      "/dist",
    );
  }

  ignoreFiles.forEach(file => {
    if (new RegExp(`^${file}`, "m").test(gitignoreSource)) {
      console.log(`  ✏️ Not adding '${file}' as already present`.yellow);
    }
    else {
      gitignoreSource += `${file}\n`;
      console.log(`  ✏️ Added '${file}'`.green);
    }
  });

  fs.writeFileSync(
    gitignoreFilePath,
    gitignoreSource,
    { encoding: "utf-8" },
  );
};

try {
  copyConfigs();
  configurePackageJson();
  configureGitignore();

  console.log("✅ Done!".green.bold);
}
catch(error) {
  console.error(`☠️ ${error.message}`.red.underline);
  console.log("❌ Failed!".red.bold);
};